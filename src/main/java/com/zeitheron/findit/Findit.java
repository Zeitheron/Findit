package com.zeitheron.findit;

import org.apache.logging.log4j.Logger;

import com.zeitheron.findit.proxy.CommonProxy;

import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

@Mod(modid = InfoFI.MOD_ID, name = InfoFI.MOD_NAME, version = InfoFI.MOD_VERSION, dependencies = "required-after:hammercore", guiFactory = "com.zeitheron.findit.cfg.CFGFactoryFI")
public class Findit
{
	@Instance
	public static Findit instance;
	
	@SidedProxy(serverSide = "com.zeitheron.findit.proxy.CommonProxy", clientSide = "com.zeitheron.findit.proxy.ClientProxy")
	public static CommonProxy proxy;
	
	public static Logger LOG;
	
	@EventHandler
	public void preInit(FMLPreInitializationEvent e)
	{
		LOG = e.getModLog();
		proxy.registerMCF(MinecraftForge.EVENT_BUS);
	}
	
	@EventHandler
	public void init(FMLInitializationEvent e)
	{
		proxy.init();
	}
}
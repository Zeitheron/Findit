package com.zeitheron.findit.proxy;

import com.zeitheron.findit.Findit;

import net.minecraftforge.fml.common.eventhandler.EventBus;

public class CommonProxy
{
	public void registerMCF(EventBus bus)
	{
	}
	
	public void init()
	{
		Findit.LOG.warn("Warning: Running on server, but this mod is client-side only! We won't crash, but just load it up.");
	}
}
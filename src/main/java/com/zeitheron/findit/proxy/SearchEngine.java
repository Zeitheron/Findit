package com.zeitheron.findit.proxy;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiPredicate;
import java.util.function.Predicate;

import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.I18n;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.ModContainer;
import net.minecraftforge.oredict.OreDictionary;

public class SearchEngine
{
	public static final Map<String, BiPredicate<String, ItemStack>> searching = new HashMap<>();
	
	static
	{
		init();
	}
	
	public static void init()
	{
		searching.put("%", (word, stack) ->
		{
			for(CreativeTabs tab : stack.getItem().getCreativeTabs())
				if(I18n.format(tab.getTranslatedTabLabel()).toLowerCase().contains(word))
					return true;
			return false;
		});
		
		searching.put("$", (word, stack) ->
		{
			for(int oid : OreDictionary.getOreIDs(stack))
			{
				String od = OreDictionary.getOreName(oid);
				if(od.toLowerCase().contains(word))
					return true;
			}
			return false;
		});
		
		searching.put("@", (word, stack) ->
		{
			ModContainer owner = Loader.instance().getIndexedModList().get(stack.getItem().getRegistryName().getResourceDomain());
			return owner != null && (owner.getModId().toLowerCase().contains(word) || owner.getName().toLowerCase().contains(word));
		});
	}
	
	private static ITooltipFlag getFlag()
	{
		return Minecraft.getMinecraft().gameSettings.advancedItemTooltips ? ITooltipFlag.TooltipFlags.ADVANCED : ITooltipFlag.TooltipFlags.NORMAL;
	}
	
	public static Predicate<ItemStack> matcher(String word)
	{
		return stack -> matches(word, stack);
	}
	
	public static boolean matches(String word, ItemStack stack)
	{
		word = word.toLowerCase();
		boolean invert = word.startsWith("!");
		if(invert)
			word = word.substring(1);
		
		List<String> fullDesc = stack.getTooltip(Minecraft.getMinecraft().player, getFlag());
		
		boolean res = false;
		
		for(String token : searching.keySet())
			if(word.startsWith(token))
			{
				String w = word.substring(token.length());
				try
				{
					if(searching.get(token).test(w, stack))
					{
						res = true;
						break;
					}
				} catch(Throwable err)
				{
				}
			}
		
		if(!res)
			for(String ln : fullDesc)
				if(ln.toLowerCase().contains(word))
				{
					res = true;
					break;
				}
			
		return invert ? !res : res;
	}
}
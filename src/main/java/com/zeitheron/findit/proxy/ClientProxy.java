package com.zeitheron.findit.proxy;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiPredicate;
import java.util.function.Predicate;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;

import com.zeitheron.findit.InfoFI;
import com.zeitheron.findit.cfg.ConfigFI;
import com.zeitheron.hammercore.client.utils.TexturePixelGetter;
import com.zeitheron.hammercore.client.utils.UV;
import com.zeitheron.hammercore.utils.WorldUtil;
import com.zeitheron.hammercore.utils.color.ColorHelper;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.event.GuiScreenEvent;
import net.minecraftforge.client.event.GuiScreenEvent.KeyboardInputEvent;
import net.minecraftforge.client.event.GuiScreenEvent.MouseInputEvent;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.common.eventhandler.EventBus;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent.ClientTickEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent.Phase;

public class ClientProxy extends CommonProxy
{
	public static KeyBinding KEY_SEARCH;
	
	public static final Map<String, BiPredicate<String, ItemStack>> searching = new HashMap<>();
	
	@Override
	public void registerMCF(EventBus bus)
	{
		bus.register(this);
	}
	
	@Override
	public void init()
	{
		KEY_SEARCH = new KeyBinding("key.findit", Keyboard.KEY_NUMPAD9, "key.categories.misc");
		ClientRegistry.registerKeyBinding(KEY_SEARCH);
	}
	
	public Predicate<ItemStack> applier = null;
	public int mouseX, mouseY;
	public GuiContainer container;
	public boolean canSearch = false;
	public GuiTextField search = null;
	
	public boolean strong = false;
	
	public final UV found_slot_overlay = new UV(new ResourceLocation(InfoFI.MOD_ID, "textures/gui/found_overlay.png"), 0, 0, 256, 256);
	
	@SubscribeEvent(priority = EventPriority.LOWEST)
	public void renderGui(GuiScreenEvent.DrawScreenEvent.Post e)
	{
		mouseX = e.getMouseX();
		mouseY = e.getMouseY();
		
		UV found_slot_overlay = new UV(new ResourceLocation(InfoFI.MOD_ID, "textures/gui/found_overlay.png"), 0, 0, 256, 256);
		
		if(search != null)
		{
			GlStateManager.pushMatrix();
			GlStateManager.disableLighting();
			GlStateManager.translate(0, 0, 300);
			search.drawTextBox();
			GlStateManager.popMatrix();
		}
		
		GlStateManager.pushMatrix();
		GlStateManager.disableLighting();
		// GlStateManager.translate(0, 0, 300);
		if(applier != null && container != null)
			for(Slot slot : container.inventorySlots.inventorySlots)
				if(!slot.getStack().isEmpty() && applier.test(slot.getStack()))
				{
					int ax = slot.xPos + container.getGuiLeft();
					int ay = slot.yPos + container.getGuiTop();
					
					boolean selected = container.getSlotUnderMouse() == slot;
					
					float mult = 1.5F;
					for(int i = 0; i < 2; ++i)
					{
						double s = 14 + ((1 - i) * 2) + (Math.sin(Math.toRadians(((System.currentTimeMillis() + slot.hashCode()) % (long) (1800 * mult)) / (5D * mult))) + 1) * 2;
						
						if(!selected)
						{
							int[] rgbs = TexturePixelGetter.getAllColors(slot.getStack());
							long offset = Math.abs(slot.hashCode());
							
							long time = Math.abs(System.currentTimeMillis() - offset);
							
							int rgb = ConfigFI.RGB_I;
							
							if(ConfigFI.colorMode < 2 && rgbs.length > 0)
							{
								int cur = (int) ((time % (1000L * rgbs.length)) / 1000L);
								float interp = (time % 1000L) / 1000F;
								cur = (cur + i) % rgbs.length;
								int argb = rgbs[cur];
								int brgb = rgbs[(cur + 1) % rgbs.length];
								rgb = ConfigFI.colorMode == 0 ? ColorHelper.interpolate(argb, brgb, interp) : argb;
							}
							
							float dir = slot.hashCode() % 2 * 2 - 1;
							float off = (slot.hashCode() % Math.abs(1 + (ConfigFI.crookedSquare ? 15 : 0))) * dir;
							
							float r = ColorHelper.getRed(rgb);
							float g = ColorHelper.getGreen(rgb);
							float b = ColorHelper.getBlue(rgb);
							GlStateManager.color(r, g, b);
							GlStateManager.pushMatrix();
							GlStateManager.translate(ax + 8, ay + 8, 0);
							GlStateManager.rotate(off, 0, 0, 1);
							GlStateManager.translate(-s / 2, -s / 2, 0);
							found_slot_overlay.render(0, 0, s, s);
							GlStateManager.popMatrix();
						}
					}
				}
		GlStateManager.popMatrix();
	}
	
	@SubscribeEvent(priority = EventPriority.HIGHEST)
	public void keyInput(KeyboardInputEvent.Pre e)
	{
		char c0 = Keyboard.getEventCharacter();
		
		if(Keyboard.getEventKey() == 0 && c0 >= ' ' || Keyboard.getEventKeyState())
		{
			int key = Keyboard.getEventKey();
			
			if(search != null && canSearch && search.textboxKeyTyped(c0, key))
			{
				updateFilter();
				e.setCanceled(true);
			}
			
			if(key == KEY_SEARCH.getKeyCode() && search == null && canSearch)
			{
				search = new GuiTextField(0, Minecraft.getMinecraft().fontRenderer, mouseX - 50, mouseY - 10, 100, 20);
				search.x = Math.max(1, Math.min(search.x, container.width - search.width - 1));
				search.y = Math.max(1, Math.min(search.y, container.height - search.height - 1));
				search.setFocused(true);
				search.setMaxStringLength(1000);
				strong = GuiScreen.isShiftKeyDown();
				updateFilter();
				e.setCanceled(true);
			}
			
			if(key == Keyboard.KEY_ESCAPE && search != null)
			{
				search = null;
				updateFilter();
				e.setCanceled(true);
			}
		}
	}
	
	@SubscribeEvent(priority = EventPriority.HIGHEST)
	public void mouseInput(MouseInputEvent.Pre e)
	{
		if(search == null || container == null)
			return;
		
		Minecraft mc = container.mc;
		int x = Mouse.getEventX() * container.width / mc.displayWidth;
		int y = container.height - Mouse.getEventY() * container.height / mc.displayHeight - 1;
		int btn = Mouse.getEventButton();
		
		if(search != null && x >= search.x && y >= search.y && x < search.x + search.width && y < search.y + search.height)
		{
			if(btn == 1 && Mouse.isButtonDown(btn))
			{
				search.setText("");
				updateFilter();
			}
			e.setCanceled(true);
		}
		
		if(Mouse.isButtonDown(btn) && search.mouseClicked(x, y, btn))
			e.setCanceled(true);
	}
	
	@SubscribeEvent
	public void clientTick(ClientTickEvent e)
	{
		if(e.phase != Phase.START)
			return;
		if(!(canSearch = Minecraft.getMinecraft().currentScreen instanceof GuiContainer) || (!strong && search != null && !search.isFocused()))
		{
			if(search != null)
			{
				search = null;
				updateFilter();
			}
		}
		container = WorldUtil.cast(Minecraft.getMinecraft().currentScreen, GuiContainer.class);
	}
	
	public void updateFilter()
	{
		if(search == null || (search != null && search.getText().isEmpty()))
		{
			applier = null;
			return;
		}
		
		String text = search.getText();
		final String[] words = text.split(" ");
		
		applier = null;
		
		for(String to : words)
		{
			Predicate<ItemStack> s = SearchEngine.matcher(to);
			if(applier == null)
				applier = s;
			else
				applier = applier.and(s);
		}
	}
}
package com.zeitheron.findit.cfg;

import com.zeitheron.findit.InfoFI;
import com.zeitheron.hammercore.cfg.HCModConfigurations;
import com.zeitheron.hammercore.cfg.IConfigReloadListener;
import com.zeitheron.hammercore.cfg.fields.ModConfigPropertyBool;
import com.zeitheron.hammercore.cfg.fields.ModConfigPropertyString;

import net.minecraftforge.common.config.Configuration;

@HCModConfigurations(modid = InfoFI.MOD_ID)
public class ConfigFI implements IConfigReloadListener
{
	public static final String[] colorTypes = { "texture_color_interpolate", "texture_color", "static_color" };
	
	@ModConfigPropertyBool(name = "Crooked", category = "Render", defaultValue = true, comment = "Enable rotation for squares on slots?")
	public static boolean crookedSquare;
	
	@ModConfigPropertyString(name = "ColorType", category = "Render", allowedValues = { "texture_color_interpolate", "texture_color", "static_color" }, comment = "What type of color should be used?", defaultValue = "texture_color_interpolate")
	public static String colorType;
	
	@ModConfigPropertyString(name = "StaticColorRGB", category = "Render", allowedValues = {}, defaultValue = "FFFFFF", comment = "Hex color of squares on slots, if static.")
	public static String rgb;
	
	public static int RGB_I;
	public static Configuration cfg;
	
	public static int colorMode;
	
	@Override
	public void reloadCustom(Configuration cfgs)
	{
		cfg = cfgs;
		try
		{
			RGB_I = Integer.parseInt(rgb, 16);
		} catch(Throwable er)
		{
			RGB_I = 0xFFFFFF;
			er.printStackTrace();
		}
		
		colorMode = -1;
		for(int i = 0; i < colorTypes.length; ++i)
			if(colorType.equals(colorTypes[i]))
			{
				colorMode = i;
				break;
			}
	}
}
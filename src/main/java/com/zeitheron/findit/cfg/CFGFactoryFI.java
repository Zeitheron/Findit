package com.zeitheron.findit.cfg;

import java.util.Set;

import com.zeitheron.findit.InfoFI;
import com.zeitheron.hammercore.HammerCore;
import com.zeitheron.hammercore.cfg.gui.HCConfigGui;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraftforge.fml.client.IModGuiFactory;

public class CFGFactoryFI implements IModGuiFactory
{
	@Override
	public void initialize(Minecraft minecraftInstance)
	{
		HammerCore.LOG.info("Created Findit! Gui Config Factory!");
	}
	
	@Override
	public boolean hasConfigGui()
	{
		return true;
	}
	
	@Override
	public GuiScreen createConfigGui(GuiScreen parentScreen)
	{
		return new HCConfigGui(parentScreen, ConfigFI.cfg, InfoFI.MOD_ID);
	}
	
	@Override
	public Set<RuntimeOptionCategoryElement> runtimeGuiCategories()
	{
		return null;
	}
}